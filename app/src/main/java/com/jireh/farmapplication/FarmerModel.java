package com.jireh.farmapplication;


import java.io.Serializable;

public class FarmerModel {


    private String farm;

    private String place;

    private String flock;

    private String age;

    private String date;

    private String st_date;

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getFlock() {
        return flock;
    }

    public void setFlock(String flock) {
        this.flock = flock;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStDate() {
        return st_date;
    }

    public void setStDate(String stDate) {
        this.st_date = stDate;
    }

}