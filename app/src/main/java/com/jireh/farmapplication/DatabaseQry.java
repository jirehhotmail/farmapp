package com.jireh.farmapplication;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

/**
 * Created by Muthamizhan C on 19-09-2017.
 */

public class DatabaseQry {
    private static final String TABLE_FARMERS = "FARMERS";
    private static final String TABLE_MEDICINE = "MEDICINE";
    private static final String TABLE_CUSTOMER = "CUSTOMER";

    private static final String COLUMN_SNO = "SNO";
    private static final String COLUMN_FARM = "FARMER";
    private static final String COLUMN_PLACE = "PLACE";
    private static final String COLUMN_FLOCK = "FLOCK";
    private static final String COLUMN_AGE = "AGE";
    private static final String COLUMN_DATE = "DATE";
    private static final String COLUMN_STDATE = "STDATE";

    private static final String COLUMN_CATEGORY = "CATEGORY";
    private static final String COLUMN_CODE = "CODE";
    private static final String COLUMN_DESCRIPTION = "DESCRIPTION";
    private static final String COLUMN_SUNITS = "SUNITS";

    private static final String COLUMN_CUSTOMER = "CUSTOMER";

    public void insertInToFarmerTable(ArrayList<FarmerModel> farmers, Context context) {
        String sql = "INSERT OR REPLACE INTO " + TABLE_FARMERS + " VALUES(?,?,?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();

        for (int idx = 0; idx < farmers.size(); idx++) {
            statement.clearBindings();
            statement.bindString(2, farmers.get(idx).getFarm());
            statement.bindString(3, farmers.get(idx).getPlace());
            statement.bindString(4, farmers.get(idx).getFlock());
            statement.bindString(5, farmers.get(idx).getAge());
            statement.bindString(6, farmers.get(idx).getDate());
            statement.bindString(7, farmers.get(idx).getStDate());


            statement.execute();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public void insertInToMedicineTable(ArrayList<MedicalModel> medicines, Context context) {
        String sql = "INSERT OR REPLACE INTO " + TABLE_MEDICINE + " VALUES(?,?,?,?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();

        for (int idx = 0; idx < medicines.size(); idx++) {
            statement.clearBindings();
            statement.bindString(2, medicines.get(idx).getCategory());
            statement.bindString(3, medicines.get(idx).getCode());
            statement.bindString(4, medicines.get(idx).getDescription());
            statement.bindString(5, medicines.get(idx).getSunits());
            statement.execute();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public void insertInToCustomerTable(ArrayList<String> customers, Context context) {
        String sql = "INSERT OR REPLACE INTO " + TABLE_CUSTOMER + " VALUES(?,?)";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();

        for (int idx = 0; idx < customers.size(); idx++) {
            statement.clearBindings();
            statement.bindString(2, customers.get(idx));
            statement.execute();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public FarmerModel getFarmerDetailsByName(Context context,String farmerName) {

        String sql = "SELECT * FROM " + TABLE_FARMERS + " WHERE FARMER ='"+farmerName +"' COLLATE NOCASE" ;
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        FarmerModel farmer = new FarmerModel();
        if (cur.moveToFirst()) {
            do {

                farmer.setFarm(cur.getString(cur.getColumnIndex(COLUMN_FARM)));
                farmer.setPlace(cur.getString(cur.getColumnIndex(COLUMN_PLACE)));
                farmer.setFlock(cur.getString(cur.getColumnIndex(COLUMN_FLOCK)));
                farmer.setAge(cur.getString(cur.getColumnIndex(COLUMN_AGE)));
                farmer.setDate(cur.getString(cur.getColumnIndex(COLUMN_DATE)));
                farmer.setStDate(cur.getString(cur.getColumnIndex(COLUMN_STDATE)));



            } while (cur.moveToNext());
        }
        return farmer;
    }

    public ArrayList<String> getAllTheCustomers(Context context) {
        String sql = "SELECT CUSTOMER FROM " + TABLE_CUSTOMER + ";";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<String> allCustomers = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {

                allCustomers.add(cur.getString(cur.getColumnIndex(COLUMN_CUSTOMER)));
            } while (cur.moveToNext());
        }
        return allCustomers;
    }

    public ArrayList<String> getAllCategorybyTitle(Context context, String categoryTitle) {
        //  select code from medicine where category = 'Broiler Feed';
        String sql = "SELECT CODE FROM " + TABLE_MEDICINE + " WHERE CATEGORY =" + "'" + categoryTitle + "' COLLATE NOCASE";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<String> allMedicines = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {

                allMedicines.add(cur.getString(cur.getColumnIndex(COLUMN_CODE)));

            } while (cur.moveToNext());
        }
        return allMedicines;
    }


    public ArrayList<String> getAllTheFarmers(Context context) {
        String sql = "SELECT FARMER FROM " + TABLE_FARMERS + ";";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<String> allFarmers = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {

                allFarmers.add(cur.getString(cur.getColumnIndex(COLUMN_FARM)));
            } while (cur.moveToNext());
        }
        return allFarmers;
    }

    public ArrayList<MedicalModel> getAllMedicalsbyTitle(Context context, String categoryTitle) {

        String sql = "SELECT * FROM " + TABLE_MEDICINE + " WHERE CATEGORY = '" + categoryTitle +"' COLLATE NOCASE";
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, null);
        ArrayList<MedicalModel> allMedicines = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {
                MedicalModel medical = new MedicalModel();
                medical.setCategory(cur.getString(cur.getColumnIndex(COLUMN_CATEGORY)));
                medical.setCode(cur.getString(cur.getColumnIndex(COLUMN_CODE)));
                medical.setDescription(cur.getString(cur.getColumnIndex(COLUMN_DESCRIPTION)));
                medical.setSunits(cur.getString(cur.getColumnIndex(COLUMN_SUNITS)));

                allMedicines.add(medical);

            } while (cur.moveToNext());
        }
        return allMedicines;
    }

    public void deleteFarmers(Context context) {
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();

        db.execSQL("DELETE  FROM " + TABLE_FARMERS);
        db.close();
    }

    public void deleteMedicines(Context context) {
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();

        db.execSQL("DELETE  FROM " + TABLE_MEDICINE);
        db.close();
    }

    public void deleteCustomers(Context context) {
        Database database = Database.getDbInstance(context);
        SQLiteDatabase db = database.getWritableDatabase();

        db.execSQL("DELETE  FROM " + TABLE_CUSTOMER);
        db.close();
    }
}

