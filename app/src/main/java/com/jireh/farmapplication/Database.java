package com.jireh.farmapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by Muthamizhan C on 19-09-2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "FARM_DB.db";

    //All table
    private static final String TABLE_FARMERS = "FARMERS";
    private static final String TABLE_MEDICINE = "MEDICINE";
    private static final String TABLE_CUSTOMER = "CUSTOMER";
    //farmer table
    private static final String COLUMN_SNO = "SNO";
    private static final String COLUMN_FARM = "FARMER";
    private static final String COLUMN_PLACE = "PLACE";
    private static final String COLUMN_FLOCK = "FLOCK";
    private static final String COLUMN_AGE = "AGE";
    private static final String COLUMN_DATE = "DATE";
    private static final String COLUMN_STDATE = "STDATE";

    //category table
    private static final String COLUMN_CATEGORY = "CATEGORY";
    private static final String COLUMN_CODE = "CODE";
    private static final String COLUMN_DESCRIPTION = "DESCRIPTION";
    private static final String COLUMN_SUNITS = "SUNITS";

    //customer table
    private static final String COLUMN_CUSTOMER = "CUSTOMER";
    public static String DB_PATH = " ";
    private static Database dbInstance = null;
    public SQLiteDatabase myDataBase;
    private Context mycontext;

    public Database(Context context) throws IOException {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mycontext = context;
        boolean dbexist = checkdatabase();
        if (dbexist) {
            System.out.println("Database exists");
            try {
                opendatabase();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Database doesn't exist");
            createdatabase();
        }
    }

    public static Database getDbInstance(Context context) {

        File outFile = context.getDatabasePath(DATABASE_NAME);
        DB_PATH = outFile.getPath();
        if (dbInstance == null) {
            try {
                dbInstance = new Database(context.getApplicationContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dbInstance;
    }

    public void createdatabase() throws IOException {
        boolean dbexist = checkdatabase();
        if (dbexist) {
            System.out.println(" Database exists.");
        } else {
            this.getReadableDatabase().close();
            try {
                if (!Prefs.getBoolean("DB3", false))
                    copydatabase();
            } catch (Exception e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkdatabase() {

        boolean checkdb = false;
        try {
            String myPath = DB_PATH;
            File dbfile = new File(myPath);
            System.out.println("database path " + myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
        }
        return checkdb;
    }

    private void copydatabase() {

        //Open your local db as the input stream
        InputStream myinput = null;
        try {
            myinput = mycontext.getAssets().open(DATABASE_NAME);


            // Path to the just created empty db
            String outfilename = DB_PATH;
            File databaseFile = new File(DB_PATH);
            // check if databases folder exists, if not create one and its subfolders
            try {
                if (!databaseFile.exists()) {
                    databaseFile.mkdir();
                    System.out.println("directory created");
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("error in cpying 2");
            }
            //Open the empty db as the output stream
            OutputStream myoutput = new FileOutputStream(outfilename);

            // transfer byte to inputfile to outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myinput.read(buffer)) > 0) {
                myoutput.write(buffer, 0, length);
            }

            //Close the streams
            myoutput.flush();
            myoutput.close();
            myinput.close();
            Prefs.putBoolean("DB3", true);
        } catch (Exception e) {
            System.out.println("error in cpying 1");
            e.printStackTrace();
        }
    }

    public void opendatabase() throws SQLException {
        //Open the database
        String mypath = DB_PATH;
        myDataBase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public synchronized void close() {
        if (myDataBase != null) {
            myDataBase.close();
        }
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
/*
        String create_table_farmers = "CREATE TABLE " + TABLE_FARMERS + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_FARM + " VARCHAR, "
                + COLUMN_PLACE + " VARCHAR, "
                + COLUMN_FLOCK + " VARCHAR, "
                + COLUMN_AGE + " VARCHAR, "
                + COLUMN_DATE + " VARCHAR, "
                + COLUMN_STDATE + " VARCHAR " + ")";


        String create_table_medicine = "CREATE TABLE " + TABLE_MEDICINE + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_CATEGORY + " VARCHAR, "
                + COLUMN_CODE + " VARCHAR, "
                + COLUMN_DESCRIPTION + " VARCHAR, "
                + COLUMN_SUNITS + " VARCHAR " + ")";

        String create_table_customer = "CREATE TABLE " + TABLE_CUSTOMER + "("
                + COLUMN_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_CUSTOMER + " VARCHAR " + ")";

        db.execSQL(create_table_farmers);
        db.execSQL(create_table_medicine);
        db.execSQL(create_table_customer);
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    /*    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FARMERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
        onCreate(db);*/


    }
}
