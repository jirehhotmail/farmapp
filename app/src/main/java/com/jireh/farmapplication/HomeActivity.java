package com.jireh.farmapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findViewById(R.id.daily_monitor).setOnClickListener(new C02001());
        findViewById(R.id.medicine).setOnClickListener(new C02012());
        findViewById(R.id.sales).setOnClickListener(new C02023());
        findViewById(R.id.logout).setOnClickListener(new C02034());
    }

    private boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    class C02001 implements OnClickListener {
        C02001() {
        }

        public void onClick(View view) {
            //check net available if yes move to next data entry activity else show toast no internet and move to data entry activity
            //mode is zero
            if (HomeActivity.this.isNetworkAvailable()) {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 0);
                // intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                return;
            } else {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 0);
                // intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                Toast.makeText(HomeActivity.this, "No Internet connection, your data will stored offline", Toast.LENGTH_LONG).show();
                return;


            }

        }
    }

    class C02012 implements OnClickListener {
        C02012() {
        }

        public void onClick(View view) {
            //check net available if yes move to next data entry activity else show toast no internet and move to data entry activity
            //mode is one
            if (HomeActivity.this.isNetworkAvailable()) {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 1);
                // intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                return;
            } else {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 1);
                // intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                Toast.makeText(HomeActivity.this, "No Internet connection, your data will stored offline", Toast.LENGTH_LONG).show();
                return;
            }

        }
    }

    class C02023 implements OnClickListener {
        C02023() {
        }

        public void onClick(View view) {
            //check net available if yes move to next data entry activity else show toast no internet and move to data entry activity
            //mode is two
            if (HomeActivity.this.isNetworkAvailable()) {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 2);
                //intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                return;
            } else {
                Intent intent = new Intent(HomeActivity.this, DataEntryActivity.class);
                intent.putExtra("mode", 2);
                //intent.putStringArrayListExtra("feeds", HomeActivity.this.getIntent().getStringArrayListExtra("feeds"));
                HomeActivity.this.startActivity(intent);
                Toast.makeText(HomeActivity.this, "No Internet connection, your data will stored offline", Toast.LENGTH_LONG).show();
                return;
            }

        }
    }

    class C02034 implements OnClickListener {
        C02034() {
        }

        public void onClick(View view) {
            //if logout remove the username and password
            Editor editor = HomeActivity.this.getApplicationContext().getSharedPreferences("prefs", 0).edit();
            editor.remove("username");
            editor.remove("password");
            editor.commit();
            HomeActivity.this.startActivity(new Intent(HomeActivity.this, MainActivity.class));
            HomeActivity.this.finish();
        }
    }
}
