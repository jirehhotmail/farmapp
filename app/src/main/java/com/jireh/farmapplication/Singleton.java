package com.jireh.farmapplication;

import android.app.Application;

import com.pixplicity.easyprefs.library.Prefs;

/**
 * Created by Muthamizhan C on 19-09-2017.
 */

public class Singleton extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Prefs.initPrefs(this);
    }
}
