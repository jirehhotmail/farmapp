package com.jireh.farmapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private Button mLoginButton;
    private EditText mPasswordField;
    private EditText mUsernameField;
    private ArrayList<String> mCustomers = new ArrayList();

    static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);//Remove title bar
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
        }

        this.mUsernameField = (EditText) findViewById(R.id.user_name);
        this.mPasswordField = (EditText) findViewById(R.id.password);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("prefs", 0);
        this.mLoginButton = (Button) findViewById(R.id.login);
        //user logged in with the username and password entered by user
        this.mLoginButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                //get the user name
                final String userName = MainActivity.this.mUsernameField.getText().toString();
                if (userName == null || userName.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter username", Toast.LENGTH_LONG).show();
                    return;
                }
                //get the password
                final String password = MainActivity.this.mPasswordField.getText().toString();
                if (password == null || password.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter password", Toast.LENGTH_LONG).show();
                } else if (isNetworkAvailable()) {
                    final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                    dialog.setMessage("Please wait...");
                    dialog.getWindow().clearFlags(2);

                    dialog.show();
                    MainActivity.this.findViewById(R.id.splash_screen).setVisibility(View.VISIBLE);
                    //run the api in thread
                    new Thread() {

                        public void run() {

                            class Login implements Runnable {

                                Login() {
                                }

                                public void run() {
                                    Toast.makeText(MainActivity.this, "Invalid credentials..", Toast.LENGTH_LONG).show();
                                    MainActivity.this.findViewById(R.id.splash_screen).setVisibility(View.GONE);
                                    dialog.dismiss();
                                }
                            }

                            class Dismiss implements Runnable {
                                Dismiss() {
                                }

                                public void run() {

                                    ///   dialog.dismiss();


                                }
                            }
                              //login with the url
                            String result = MainActivity.this.getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=checklogin&username=" + userName + "&password=" + password).replace(" ", "%20"));
                            Log.i("MainActivity ", "Login Url" + " " + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=checklogin&username=" + userName + "&password=" + password).replace(" ", "%20"));
                            Log.i("MainActivity ", "Login" + result);
                            if (result == null || !result.equals("1")) {
                                MainActivity.this.runOnUiThread(new Login());
                            } else {
                                //store the user name and password in the shared preference if logged in with Online
                                Editor editor = prefs.edit();
                                editor.putString("username", userName);
                                editor.putString("password", password);
                                editor.commit();

                                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                MainActivity.this.startActivity(intent);
                                MainActivity.this.finish();
                                MainActivity.this.runOnUiThread(new Dismiss());
                            }
                        }

                    }.start();


                } else {
                    //if user logged in with offline use this credentials
                    if (mUsernameField.getText().toString().equals("laxmifarms") && mPasswordField.getText().toString().equals("poultry1985")) {
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        MainActivity.this.startActivity(intent);
                        MainActivity.this.finish();
                    } else {
                        Toast.makeText(MainActivity.this, "Invalid credentials..", Toast.LENGTH_LONG).show();

                    }


                }
            }
        });
        if (prefs.contains("username") && prefs.contains("password")) {
            this.mUsernameField.setText(prefs.getString("username", "poojanew"));
            this.mPasswordField.setText(prefs.getString("password", "poojanew"));
            this.mLoginButton.performClick();
        }
        this.mPasswordField.setOnEditorActionListener(new C02082());



    }

    private void exportDb(String s) {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/FARM_DB";
                String backupDBPath = "FARM_DB.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }


    private String capitalize(String s) {

        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else
            return Character.toUpperCase(first) + s.substring(1);


    }

    private void testLink(final String link) {
        new Thread() {
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) new URL(link).openConnection();
                    Log.i("ssample", "received : " + MainActivity.convertStreamToString(new BufferedInputStream(urlConnection.getInputStream())));
                    urlConnection.disconnect();
                } catch (Exception e) {
                } catch (Throwable th) {
                    urlConnection.disconnect();
                }
            }
        }.start();
    }

    private String getResult(String link) {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) new URL(link).openConnection();
            String convertStreamToString = convertStreamToString(new BufferedInputStream(urlConnection.getInputStream()));
            urlConnection.disconnect();
            return convertStreamToString;
        } catch (Exception e) {
            return null;
        } catch (Throwable th) {
            urlConnection.disconnect();
            return null;
        }
    }

    private boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    class C02082 implements OnEditorActionListener {
        C02082() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId != 6) {
                return false;
            }
            MainActivity.this.mLoginButton.performClick();
            return true;
        }
    }
}
