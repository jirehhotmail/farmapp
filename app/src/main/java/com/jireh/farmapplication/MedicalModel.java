package com.jireh.farmapplication;

/**
 * Created by Muthamizhan C on 19-09-2017.
 */


import java.io.Serializable;

public class MedicalModel {


    private String category;

    private String code;

    private String description;

    private String sunits;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSunits() {
        return sunits;
    }

    public void setSunits(String sunits) {
        this.sunits = sunits;
    }

}

