package com.jireh.farmapplication;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class DataEntryActivity extends AppCompatActivity {
    Date date;
    int dayOfMonth;
    int month;
    List<String> dialy_monitor_offline;
    List<String> medicine_vacc_offline;
    List<String> bird_sale_offline;
    private ArrayList<String> farmers = new ArrayList();
    private ArrayList<String> feeds = new ArrayList();
    private View invisibleView = null;
    private EditText mAge;
    private EditText mAmount;
    private EditText mBirdsSold;
    private Spinner mCode;
    private Spinner mCustomer;
    private ArrayList<String> mCustomers = new ArrayList();
    private Set<String> mDailyEntries = new HashSet();
    private EditText mDate;
    private DatePickerDialog mDatePickerDlg;
    private String mDateText;
    private boolean mFarmerLoaded = false;
    private Spinner mFarmerSelection;
    private Spinner mFeedSelection;
    private boolean mFirstTime = true;
    private EditText mFlock;
    private ArrayList<String> mMedicines = new ArrayList();
    private int mMode = -1;
    private EditText mMortality;
    private String mPassword;
    private EditText mPrice;
    private ProgressDialog mProgress;
    private EditText mQuantity;
    private SharedPreferences mSharedPrefs;
    private EditText mUnits;
    private String mUsername;
    private OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
            if (DataEntryActivity.this.mFirstTime) {
                DataEntryActivity.this.mFirstTime = false;
                DataEntryActivity.this.mProgress.dismiss();
                return;
            }
            if (DataEntryActivity.this.invisibleView != null) {
                DataEntryActivity.this.invisibleView.setVisibility(View.VISIBLE);
                DataEntryActivity.this.invisibleView = null;
            }
            DataEntryActivity.this.mFarmerLoaded = true;
            DataEntryActivity.this.mProgress.show();
            new Thread() {
                public void run() {
                    DataEntryActivity.this.loadFarmer(mFarmerSelection.getSelectedItem().toString());
                }
            }.start();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    };
    private ArrayList<String> mVaccines = new ArrayList();
    private EditText mWater;
    private EditText mWeight;
    private TextWatcher mPriceTextWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            DataEntryActivity.this.mAmount.setText("0");
            if (!DataEntryActivity.this.mPrice.getText().toString().isEmpty() && !DataEntryActivity.this.mWeight.getText().toString().isEmpty()) {
                try {
                    DataEntryActivity.this.mAmount.setText("" + (Long.parseLong(DataEntryActivity.this.mPrice.getText().toString()) * Long.parseLong(DataEntryActivity.this.mWeight.getText().toString())));
                } catch (Exception e) {
                }
            }
        }

        public void afterTextChanged(Editable s) {
        }
    };

    static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.mSharedPrefs = getSharedPreferences("prefs", 0);
        this.mDailyEntries = this.mSharedPrefs.getStringSet("dailyEntries", this.mDailyEntries);
        this.mProgress = new ProgressDialog(this);
        this.mProgress.setMessage("Please wait...");
        this.mProgress.getWindow().clearFlags(2);
        this.mMode = getIntent().getIntExtra("mode", 0);
        this.mUsername = getApplicationContext().getSharedPreferences("prefs", 0).getString("username", "poojanew");
        this.mPassword = getApplicationContext().getSharedPreferences("prefs", 0).getString("password", "poojanew");
        // this.feeds = getIntent().getStringArrayListExtra("feeds");
        Log.i("ssample", "username and password are " + this.mUsername + "," + this.mPassword);
        LayoutParams params;


        if (this.mMode == 0) {
            setContentView(R.layout.daily_monitor_layout);
            this.mFlock = (EditText) findViewById(R.id.select_flock);
            this.mDate = (EditText) findViewById(R.id.date);
            this.mAge = (EditText) findViewById(R.id.age);
            this.mMortality = (EditText) findViewById(R.id.mortality);
            this.mQuantity = (EditText) findViewById(R.id.quantity);
            this.mWater = (EditText) findViewById(R.id.water);
            this.mWeight = (EditText) findViewById(R.id.body_weight);
            findViewById(R.id.submit).setOnClickListener(new C01851());
            this.mProgress.show();
            findViewById(R.id.edit_layout).setVisibility(View.GONE);
            params = (LayoutParams) findViewById(R.id.splash_screen).getLayoutParams();
            params.width = getResources().getDisplayMetrics().widthPixels;
            params.height = getResources().getDisplayMetrics().heightPixels;
            findViewById(R.id.splash_screen).setVisibility(View.VISIBLE);
            //check if network available
            if (isNetworkAvailable()) {
                //update the database from server()
                updateServer();
            } else {
                new C01862().start();
            }


        } else if (this.mMode == 1) {

            setContentView(R.layout.medicine_vaccination_layout);


            this.mFlock = (EditText) findViewById(R.id.select_flock);
            this.mDate = (EditText) findViewById(R.id.date);
            this.mCode = (Spinner) findViewById(R.id.code);
            this.mQuantity = (EditText) findViewById(R.id.quantity);
            this.mDate.setOnTouchListener(new C01883());
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, new String[]{"Medicines", "Vaccines"});
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ((Spinner) findViewById(R.id.select_category)).setAdapter(adapter);
            ((Spinner) findViewById(R.id.select_category)).setOnItemSelectedListener(new C01894());
            findViewById(R.id.submit).setOnClickListener(new C01925());
            this.mProgress.show();
            findViewById(R.id.edit_layout).setVisibility(View.GONE);
            params = (LayoutParams) findViewById(R.id.splash_screen).getLayoutParams();
            params.width = getResources().getDisplayMetrics().widthPixels;
            params.height = getResources().getDisplayMetrics().heightPixels;
            findViewById(R.id.splash_screen).setVisibility(View.VISIBLE);

            this.mUnits = (EditText) findViewById(R.id.units);
            date = new Date();
            dayOfMonth = date.getDate();
            month = date.getMonth();
            this.mDateText = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + (date.getYear() + 1900);
            this.mDate.setText(this.mDateText);
            //check if network available
            if (isNetworkAvailable()) {
                //update the database from server()
                updateServer();
            } else {
                new C01936().start();
            }
        } else {
            setContentView(R.layout.bird_sales_layout);


            this.mFlock = (EditText) findViewById(R.id.select_flock);
            this.mDate = (EditText) findViewById(R.id.date);
            this.mDate.setOnTouchListener(new C01957());
            this.mCustomer = (Spinner) findViewById(R.id.customer);
            this.mBirdsSold = (EditText) findViewById(R.id.sold);
            this.mWeight = (EditText) findViewById(R.id.weight);
            this.mPrice = (EditText) findViewById(R.id.price);
            this.mWeight.addTextChangedListener(this.mPriceTextWatcher);
            this.mPrice.addTextChangedListener(this.mPriceTextWatcher);
            this.mAmount = (EditText) findViewById(R.id.amount);
            date = new Date();
            dayOfMonth = date.getDate();
            month = date.getMonth();
            this.mDateText = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + (date.getYear() + 1900);
            this.mDate.setText(this.mDateText);
            this.mProgress.show();
            findViewById(R.id.edit_layout).setVisibility(View.GONE);
            params = (LayoutParams) findViewById(R.id.splash_screen).getLayoutParams();
            params.width = getResources().getDisplayMetrics().widthPixels;
            params.height = getResources().getDisplayMetrics().heightPixels;
            findViewById(R.id.splash_screen).setVisibility(View.VISIBLE);

            findViewById(R.id.submit).setOnClickListener(new C01999());
            //check if network available
            if (isNetworkAvailable()) {
                //update the database from server()
                updateServer();
            } else {
                new C01968().start();
            }

        }

        //push offline data to server

        if (DataEntryActivity.this.isNetworkAvailable()) {
            DataEntryActivity.this.mProgress.show();
            SharedPreferences prefGetDM = getSharedPreferences("DailyMonitorPrefs", Context.MODE_PRIVATE);
            Set<String> allsetValuesDM = prefGetDM.getStringSet("daily_monitor", null);
            if (allsetValuesDM != null) {
                dialy_monitor_offline = new ArrayList<String>(allsetValuesDM);
                for (int idx = 0; idx < dialy_monitor_offline.size(); idx++) {
                    System.out.println("daily_monitor values final::" + dialy_monitor_offline.get(idx));
                }

                DataEntryActivity.this.mProgress.show();
                for (int idx = 0; idx < dialy_monitor_offline.size(); idx++) {


                    final int finalIdx = idx;
                    new Thread() {

                        public void run() {

                            try {
                                HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveDetails.php?type=savebde&username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveDetails.php?type=savebde&username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                urlConnection.setDoOutput(true);
                                urlConnection.setRequestMethod("POST");
                                urlConnection.setUseCaches(false);
                                urlConnection.setConnectTimeout(10000);
                                urlConnection.setReadTimeout(10000);
                                urlConnection.setRequestProperty("Content-Type", "application/json");
                                urlConnection.connect();
                                DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                printout.writeBytes(dialy_monitor_offline.get(finalIdx));
                                printout.flush();
                                printout.close();
                                if (urlConnection.getResponseCode() == 200) {
                                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    StringBuffer sb = new StringBuffer();
                                    while (true) {
                                        String line = br.readLine();
                                        if (line == null) {
                                            break;
                                        }
                                        sb.append(line + "\n");
                                    }
                                    br.close();
                                    Log.i("ssample", "res " + sb.toString());
                                } else {
                                    Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.i("ssample", "Exception...");
                            }
                            DataEntryActivity.this.runOnUiThread(new C01821());
                        }

                        class C01821 implements Runnable {
                            C01821() {
                            }

                            public void run() {
                                DataEntryActivity.this.mProgress.cancel();


                            }
                        }
                    }.start();
                }
                Toast.makeText(DataEntryActivity.this, "Your offline data submitted successfully", Toast.LENGTH_LONG).show();
                SharedPreferences pref = getSharedPreferences("DailyMonitorPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.remove("daily_monitor");
                editor.commit();

            }


            ///medicine vaccination
            SharedPreferences prefGetMV = getSharedPreferences("MedicineVacPrefs", Context.MODE_PRIVATE);
            Set<String> allsetValuesMV = prefGetMV.getStringSet("medicine_vaccination", null);
            if (allsetValuesMV != null) {
                medicine_vacc_offline = new ArrayList<String>(allsetValuesMV);
                for (int idx = 0; idx < medicine_vacc_offline.size(); idx++) {
                    System.out.println("medicine_vaccination values final::" + medicine_vacc_offline.get(idx));
                }

                DataEntryActivity.this.mProgress.show();
                for (int idx = 0; idx < medicine_vacc_offline.size(); idx++) {
                    final int finalIdx = idx;
                    DataEntryActivity.this.mProgress.show();


                    new Thread() {

                        public void run() {
                            try {
                                HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveMed.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveMed.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                urlConnection.setDoOutput(true);
                                urlConnection.setRequestMethod("POST");
                                urlConnection.setUseCaches(false);
                                urlConnection.setConnectTimeout(10000);
                                urlConnection.setReadTimeout(10000);
                                urlConnection.setRequestProperty("Content-Type", "application/json");
                                urlConnection.connect();
                                DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                printout.writeBytes(medicine_vacc_offline.get(finalIdx));
                                printout.flush();
                                printout.close();
                                if (urlConnection.getResponseCode() == 200) {
                                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    StringBuffer sb = new StringBuffer();
                                    while (true) {
                                        String line = br.readLine();
                                        if (line == null) {
                                            break;
                                        }
                                        sb.append(line + "\n");
                                    }
                                    br.close();
                                    Log.i("ssample", "res " + sb.toString());
                                } else {
                                    Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.i("ssample", "Exception...");
                            }
                            DataEntryActivity.this.runOnUiThread(new C01901());
                        }

                        class C01901 implements Runnable {
                            C01901() {
                            }

                            public void run() {
                                DataEntryActivity.this.mProgress.cancel();

                            }
                        }
                    }.start();


                }
                Toast.makeText(DataEntryActivity.this, "Your offline data submitted successfully", Toast.LENGTH_LONG).show();
                SharedPreferences pref = getSharedPreferences("MedicineVacPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.remove("medicine_vaccination");
                editor.commit();
            }


            //bird sale
            SharedPreferences prefGetBS = getSharedPreferences("BirdSalesPrefs", Context.MODE_PRIVATE);
            Set<String> allsetValuesBS = prefGetBS.getStringSet("bird_sales", null);
            if (allsetValuesBS != null) {

                bird_sale_offline = new ArrayList<String>(allsetValuesBS);

                for (int idx = 0; idx < bird_sale_offline.size(); idx++) {
                    System.out.println("bird_sales values final::" + bird_sale_offline.get(idx));
                }

                DataEntryActivity.this.mProgress.show();
                for (int idx = 0; idx < bird_sale_offline.size(); idx++) {

                    DataEntryActivity.this.mProgress.dismiss();
                    final int finalIdx = idx;
                    new Thread() {

                        public void run() {
                            try {
                                HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveBirdSale.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveBirdSale.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                urlConnection.setDoOutput(true);
                                urlConnection.setRequestMethod("POST");
                                urlConnection.setUseCaches(false);
                                urlConnection.setConnectTimeout(10000);
                                urlConnection.setReadTimeout(10000);
                                urlConnection.setRequestProperty("Content-Type", "application/json");
                                urlConnection.connect();
                                DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                printout.writeBytes(bird_sale_offline.get(finalIdx));
                                printout.flush();
                                printout.close();
                                if (urlConnection.getResponseCode() == 200) {
                                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    StringBuffer sb = new StringBuffer();
                                    while (true) {
                                        String line = br.readLine();
                                        if (line == null) {
                                            break;
                                        }
                                        sb.append(line + "\n");
                                    }
                                    br.close();
                                    Log.i("ssample", "res " + sb.toString());
                                } else {
                                    Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.i("ssample", "Exception...");
                            }
                            DataEntryActivity.this.runOnUiThread(new C01971());
                        }

                        class C01971 implements Runnable {
                            C01971() {
                            }

                            public void run() {
                                DataEntryActivity.this.mProgress.cancel();

                            }
                        }
                    }.start();
                }
                Toast.makeText(DataEntryActivity.this, "Your offline data submitted successfully", Toast.LENGTH_LONG).show();
                SharedPreferences pref = getSharedPreferences("BirdSalesPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.remove("bird_sales");
                editor.commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void updateServer() {
        new Thread() {
            @Override
            public void run() {


                // get the farmer details
                String resultFarmer = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getfarm&username=" + mUsername + "&password=" + mPassword).replace(" ", "%20"));
                Log.i("MainActivity ", "get farmers Url " + " " + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getfarm&username=" + mUsername + "&password=" + mPassword).replace(" ", "%20"));
                Log.i("MainActivity ", "get farmers" + resultFarmer);


                //get the values from the database and store it locally
                JSONArray farmersArray = null;
                try {
                    farmersArray = new JSONObject(resultFarmer).getJSONArray("farms");
                    ArrayList<FarmerModel> farmers = new ArrayList();

                    for (int idx = 0; idx < farmersArray.length(); idx++) {
                        Gson gson = new Gson();

                        try {
                            FarmerModel farmer = gson.fromJson(farmersArray.get(idx).toString(), FarmerModel.class);
                            farmers.add(farmer);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    DatabaseQry database = new DatabaseQry();
                    database.deleteFarmers(DataEntryActivity.this);
                    database.insertInToFarmerTable(farmers, DataEntryActivity.this);
                    //get medicine details()
                    getMedicineDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //export the db
                //   exportDb("FARM_DB.db");


            }
        }.start();
    }

    private void getMedicineDetails() {


        //get the medicine details
        String medicines = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Medicines&username=" + mUsername + "&password=" + mPassword).replace(" ", "%20"));
        Log.i("MainActivity ", "get Medicines Url " + " " + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Medicines&username=" + mUsername + "&password=" + mPassword).replace(" ", "%20"));
        Log.i("MainActivity ", "get Medicines" + medicines);


        //get the values from the database and store it locally
        try {
            JSONArray medicineArray = new JSONObject(medicines).getJSONArray("med");

            ArrayList<MedicalModel> medicine_list = new ArrayList();

            for (int idx = 0; idx < medicineArray.length(); idx++) {
                Gson gson = new Gson();

                MedicalModel medicine = gson.fromJson(medicineArray.get(idx).toString(), MedicalModel.class);
                medicine_list.add(medicine);
            }
            DatabaseQry database_med = new DatabaseQry();
            database_med.deleteMedicines(DataEntryActivity.this);
            database_med.insertInToMedicineTable(medicine_list, DataEntryActivity.this);
            getCustomerDetails();


        } catch (JSONException e) {
            Log.i("ssample", "Eception...");
            e.printStackTrace();
        }


    }

    private void getCustomerDetails() {

        String resultCat = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Customers&username=" + mUsername + "&password=" + mPassword).replace(" ", "%20"));
        // Log.i("DataEntryActivity", "Customers url" + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Customers&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
        //  Log.i("DataEntryActivity", "Customers" + result);
        DataEntryActivity.this.mCustomers.clear();
        try {
            JSONArray array = (JSONArray) new JSONObject(resultCat).get("cust");
            int length = array.length();
            for (int i = 0; i < length; i++) {
                DataEntryActivity.this.mCustomers.add((String) ((JSONObject) array.get(i)).get("customer"));
            }
            Log.i("ssample", "mCustomers count : " + DataEntryActivity.this.mCustomers.size());

            //Inser the customer details into Customer Table
            DatabaseQry databaseQry = new DatabaseQry();
            databaseQry.deleteCustomers(DataEntryActivity.this);
            databaseQry.insertInToCustomerTable(mCustomers, DataEntryActivity.this);
            if (this.mMode == 1) {
                new C01862().start();
            } else if (this.mMode == 2) {
                new C01936().start();
            } else {
                new C01968().start();
            }

        } catch (Exception e) {
            Log.i("ssample", "exception...");
            e.printStackTrace();

        }

    }

    private void loadFarmers() {
        if (this.mMode == 0 || this.mMode == 1 || this.mMode == 2) {
            //String result = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getfarm&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
            Log.i("DataEntryActivity", "getFarmer url" + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getfarm&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
            // Log.i("DataEntryActivity", "getFarmer" + result);
            this.farmers.clear();
            try {
              /*  JSONArray array = (JSONArray) new JSONObject(result).get("farms");
                int length = array.length();
                for (int i = 0; i < length; i++) {
                    this.farmers.add(((JSONObject) array.get(i)).getString("farm"));
                }*/

                //Get the farmer names from the local database and add to farmer array list
                DatabaseQry databaseQry = new DatabaseQry();
                this.farmers.addAll(databaseQry.getAllTheFarmers(DataEntryActivity.this));
                if (this.mMode == 1) {
                    loadMedicineVaccination();
                } else if (this.mMode == 2) {
                    loadCustomers();
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        DataEntryActivity.this.mProgress.dismiss();
                        DataEntryActivity.this.findViewById(R.id.edit_layout).setVisibility(View.VISIBLE);
                        DataEntryActivity.this.findViewById(R.id.splash_screen).setVisibility(View.GONE);
                        DataEntryActivity.this.loadFarmerSpinner();
                    }
                });
            } catch (Exception e) {
                Log.i("ssample", "exception...");
                e.printStackTrace();
                finish();
            }
        }
    }

    private void loadMedicineVaccination() {
        //   String result = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Medicines&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
        //   Log.i("DataEntryActivity", "Medicine Vaccination url" + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Medicines&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
        //  Log.i("DataEntryActivity", "Medicine Vaccination" + result);
        this.mMedicines.clear();
        this.mVaccines.clear();
        try {
            //get the medicine and vaccines details and set to respective array lsit
            DatabaseQry databaseQry = new DatabaseQry();
            ArrayList<MedicalModel> medicalModel = databaseQry.getAllMedicalsbyTitle(DataEntryActivity.this, "medicines");
            ArrayList<MedicalModel> vaccines = databaseQry.getAllMedicalsbyTitle(DataEntryActivity.this, "vaccines");
            //     JSONArray array = (JSONArray) new JSONObject(result).get("med");
            //      int length = array.length();
            for (int idx = 0; idx < medicalModel.size(); idx++) {
                this.mMedicines.add(medicalModel.get(idx).getDescription() + "\n" + medicalModel.get(idx).getSunits());
            }

            for (int idx = 0; idx < vaccines.size(); idx++) {
                this.mVaccines.add(vaccines.get(idx).getDescription() + "\n" + vaccines.get(idx).getSunits());
            }
            Log.i("ssample", "medicines, vaccines count : " + this.mMedicines.size() + "," + this.mVaccines.size());
            runOnUiThread(new Runnable() {
                public void run() {
                    DataEntryActivity.this.loadCodeSpinner();
                }
            });
        } catch (Exception e) {
            Log.i("ssample", "exception...");
            e.printStackTrace();
            finish();
        }
    }

    private void loadCodeSpinner() {
        Log.i("ssample", "loadFarmerSpinner");
        final ArrayList<String> list = ((Spinner) findViewById(R.id.select_category)).getSelectedItemPosition() == 0 ? this.mMedicines : this.mVaccines;
        String[] str = new String[list.size()];
        for (int i = 0; i < str.length; i++) {
            str[i] = ((String) list.get(i)).substring(0, ((String) list.get(i)).lastIndexOf("\n"));
        }
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, str);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mCode.setAdapter(adapter);
        this.mCode.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DataEntryActivity.this.mUnits.setText(((String) list.get(position)).substring(((String) list.get(position)).lastIndexOf("\n") + 1));
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    private void loadCustomers() {
        //   String result = getResult(("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Customers&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
        // Log.i("DataEntryActivity", "Customers url" + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=Customers&username=" + this.mUsername + "&password=" + this.mPassword).replace(" ", "%20"));
        //  Log.i("DataEntryActivity", "Customers" + result);
        this.mCustomers.clear();
        try {
            //    JSONArray array = (JSONArray) new JSONObject(result).get("cust");
            //  int length = array.length();
            //  for (int i = 0; i < length; i++) {
            DatabaseQry databaseQry = new DatabaseQry();
            this.mCustomers.addAll(databaseQry.getAllTheCustomers(DataEntryActivity.this));

            //   }
            Log.i("ssample", "mCustomers count : " + this.mCustomers.size());
            runOnUiThread(new Runnable() {
                public void run() {
                    String[] str = new String[DataEntryActivity.this.mCustomers.size()];
                    for (int i = 0; i < str.length; i++) {
                        str[i] = (String) DataEntryActivity.this.mCustomers.get(i);
                    }
                    List<String> customers = Arrays.asList(str);
                    Collections.sort(customers, new Comparator<String>() {
                        @Override
                        public int compare(String string1, String string2) {
                            return string1.compareTo(string2);
                        }
                    });
                    ArrayAdapter<CharSequence> adapter = new ArrayAdapter(DataEntryActivity.this, android.R.layout.simple_spinner_item, customers);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    DataEntryActivity.this.mCustomer.setAdapter(adapter);
                }
            });
            //Inser the customer details into Customer Table
            //  DatabaseQry databaseQry =new DatabaseQry();
            databaseQry.deleteCustomers(DataEntryActivity.this);
            databaseQry.insertInToCustomerTable(mCustomers, DataEntryActivity.this);
            exportDb();
        } catch (Exception e) {
            Log.i("ssample", "exception...");
            e.printStackTrace();
            finish();
        }
    }

    private void exportDb() {


        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/FARM_DB";
                String backupDBPath = "FARM_DB.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }

    }

    private void loadFarmer(final String farmerName) {
        //  String link = String.format("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getflockandage&farm=%s&username=%s&password=%s", new Object[]{this.farmers.get(i), this.mUsername, this.mPassword}).replace(" ", "%20");
        // Log.i("DataEntryActivity", "Load farmer flock age link" + ("http://bims1.spectacularsys.com/appinit/FileToSendDetails.php?type=getflockandage&farm=" + this.farmers.get(i) + "&username=" + this.mUsername + "password=" + this.mPassword));
        //  Log.i("DataEntryActivity", "Load farmer flock age" + link);
        //   final String result = getResult(link);
        //    Log.i("ssample", "link is " + link);
        //    Log.i("ssample", "result is " + result);
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    DataEntryActivity.this.mProgress.cancel();
                    DataEntryActivity.this.findViewById(R.id.edit_layout).setVisibility(View.VISIBLE);
                    DataEntryActivity.this.findViewById(R.id.splash_screen).setVisibility(View.GONE);
                    DataEntryActivity.this.mFlock.setText("");
                    if (DataEntryActivity.this.mAge != null) {
                        DataEntryActivity.this.mAge.setText("");
                    }
                    //JSONArray array = new JSONObject(result).getJSONArray("details");
                    //  int length = array.length();
                    //  for (int i = 0; i < length; i++) {
                    //  JSONObject child = array.getJSONObject(i);


                    //get the flock,age,date details using the Farmer Name
                    DatabaseQry databaseQry = new DatabaseQry();
                    FarmerModel farmer = databaseQry.getFarmerDetailsByName(DataEntryActivity.this, farmerName);
                    DataEntryActivity.this.mFlock.setText(farmer.getFlock());
                    if (DataEntryActivity.this.mAge != null) {
                        DataEntryActivity.this.mAge.setText(farmer.getAge());
                    }
                    if (DataEntryActivity.this.mMode == 0) {
                        DataEntryActivity.this.mDate.setText(farmer.getDate());
                        DataEntryActivity.this.mDateText = farmer.getDate();
                        // }
                        //  Log.i("ssample", "flock,date,age: " + child.getString("flock") + "," + child.getString("age") + "," + child.getString("date"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadFarmerSpinner() {
        int i;
        Log.i("ssample", "loadFarmerSpinner");
        this.mFarmerSelection = (Spinner) findViewById(R.id.select_farmer);
        String[] str = new String[this.farmers.size()];
        for (i = 0; i < str.length; i++) {
            str[i] = (String) this.farmers.get(i);
        }
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, str) {
            @NonNull
            public View getView(int position, View convertView, ViewGroup parent) {
                Log.i("ssample", "getView :: position, mFarmerLoaded :: " + position + "," + DataEntryActivity.this.mFarmerLoaded);
                View v = super.getView(position, convertView, parent);
                if (!DataEntryActivity.this.mFarmerLoaded && position == 0) {
                    DataEntryActivity.this.invisibleView = v;
                    v.setVisibility(View.INVISIBLE);
                }
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                Log.i("ssample", "position, mFarmerLoaded :: " + position + "," + DataEntryActivity.this.mFarmerLoaded);
                if (convertView != null) {
                    convertView.setVisibility(View.VISIBLE);
                }
                return super.getDropDownView(position, convertView, parent);
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mFarmerSelection.setAdapter(adapter);
        ((MySpinner) this.mFarmerSelection).setOnItemSelectedEvenIfUnchangedListener(this.itemSelectedListener);
        this.mFarmerSelection.setOnItemSelectedListener(this.itemSelectedListener);
        this.mFeedSelection = (Spinner) findViewById(R.id.select_feed);
        if (this.mFeedSelection != null) {
            //get the feed details for the local db
         /*   String[] feeds = new String[this.feeds.size()];
            for (i = 0; i < feeds.length; i++) {
                feeds[i] = (String) this.feeds.get(i);
            }*/
            DatabaseQry databaseQry = new DatabaseQry();

            feeds.addAll(databaseQry.getAllCategorybyTitle(DataEntryActivity.this, "Broiler Feed"));
            Collections.sort(feeds, new Comparator<String>() {
                @Override
                public int compare(String string1, String string2) {
                    return string1.compareTo(string2);
                }
            });

            ArrayAdapter<CharSequence> adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, feeds);
            adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            this.mFeedSelection.setAdapter(adapter2);
        }
    }

    private String getResult(String link) {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) new URL(link).openConnection();
            String convertStreamToString = convertStreamToString(new BufferedInputStream(urlConnection.getInputStream()));
            urlConnection.disconnect();
            return convertStreamToString;
        } catch (Exception e) {
            Log.i("ssample", "Exception...");
            return null;
        } catch (Throwable th) {
            urlConnection.disconnect();
            return null;

        }

    }

    private boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private String getMedicineVaccineString() {
        Object obj = null;
        JSONObject obj2 = new JSONObject();
        try {
            obj2.put("farm", DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition()));
            obj2.put("flock", DataEntryActivity.this.mFlock.getText().toString());
            obj2.put("date", DataEntryActivity.this.mDate.getText().toString());
            boolean isMedicines = ((Spinner) DataEntryActivity.this.findViewById(R.id.select_category)).getSelectedItemPosition() == 0;
            obj2.put("category", isMedicines ? "Medicines" : "Vaccines");
            int codePosition = DataEntryActivity.this.mCode.getSelectedItemPosition();
            String str2 = "code";
            if (isMedicines) {
                obj = (String) DataEntryActivity.this.mMedicines.get(codePosition);
            } else {
                obj = (String) DataEntryActivity.this.mVaccines.get(codePosition);
            }
            obj2.put(str2, obj);
            obj2.put("units", DataEntryActivity.this.mUnits.getText().toString());
            obj2.put("quantity", DataEntryActivity.this.mQuantity.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String str3 = obj2.toString();
        Log.i("ssample", "posting : " + str3);
        return str3;
    }

    private String getBirdSaleString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("date", DataEntryActivity.this.mDate.getText().toString());
            obj.put("farm", DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition()));
            obj.put("flock", DataEntryActivity.this.mFlock.getText().toString());
            obj.put("customer", DataEntryActivity.this.mCustomers.get(DataEntryActivity.this.mCustomer.getSelectedItemPosition()));
            obj.put("birds", DataEntryActivity.this.mBirdsSold.getText().toString());
            obj.put("totalweight", DataEntryActivity.this.mWeight.getText().toString());
            obj.put("priceperkg", DataEntryActivity.this.mPrice.getText().toString());
            obj.put("amount", DataEntryActivity.this.mAmount.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String str = obj.toString();
        Log.i("ssample", "posting : " + str);
        return str;
    }

    @SuppressLint("AppCompatCustomView")
    public static class MySpinner extends Spinner {
        OnItemSelectedListener listener;

        public MySpinner(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setSelection(int position) {
            super.setSelection(position);
            if (this.listener != null) {
                this.listener.onItemSelected(null, null, position, 0);
            }
        }

        public void setOnItemSelectedEvenIfUnchangedListener(OnItemSelectedListener listener) {
            this.listener = listener;
        }
    }

    class C01851 implements OnClickListener {
        C01851() {
        }

        public void onClick(View view) {
            if (!DataEntryActivity.this.mFarmerLoaded) {
                Toast.makeText(DataEntryActivity.this, "Select Farmer", Toast.LENGTH_SHORT).show();
            } else if (DataEntryActivity.this.mMortality.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Mortality", Toast.LENGTH_SHORT).show();
            } else if (DataEntryActivity.this.mQuantity.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Quantity", Toast.LENGTH_SHORT).show();
            } else if (DataEntryActivity.this.mWater.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Water", Toast.LENGTH_SHORT).show();
            } else if (DataEntryActivity.this.mWeight.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Weight", Toast.LENGTH_SHORT).show();
            } else if (DataEntryActivity.this.isNetworkAvailable()) {
                try {
                    //once submit disable the submit button
                    findViewById(R.id.submit).setEnabled(false);
                    //if network available push data to server
                    final String dailyMonitorString = getDailyMonitorJsonString();

                    DataEntryActivity.this.mProgress.dismiss();
                    new Thread() {

                        public void run() {

                            Date date = new Date();
                            int dayOfMonth = date.getDate();
                            int month = date.getMonth();
                            String dateText = ((String) DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition())) + "_" + (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + (date.getYear() + 1900);

                            if (DataEntryActivity.this.mDailyEntries.contains(dateText)) {
                                Log.i("ssample", "Daily Entry is found...");
                            } else {
                                Log.i("ssample", "Daily Entry not found...");
                                DataEntryActivity.this.mDailyEntries.add(dateText);
                                Editor editor = DataEntryActivity.this.mSharedPrefs.edit();
                                editor.putStringSet("dailyEntries", DataEntryActivity.this.mDailyEntries);
                                editor.commit();
                            }
                            try {
                                HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveDetails.php?type=savebde&username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveDetails.php?type=savebde&username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                urlConnection.setDoOutput(true);
                                urlConnection.setRequestMethod("POST");
                                urlConnection.setUseCaches(false);
                                urlConnection.setConnectTimeout(10000);
                                urlConnection.setReadTimeout(10000);
                                urlConnection.setRequestProperty("Content-Type", "application/json");
                                urlConnection.connect();
                                DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                printout.writeBytes(dailyMonitorString);
                                printout.flush();
                                printout.close();
                                if (urlConnection.getResponseCode() == 200) {
                                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    StringBuffer sb = new StringBuffer();
                                    while (true) {
                                        String line = br.readLine();
                                        if (line == null) {
                                            break;
                                        }
                                        sb.append(line + "\n");
                                    }
                                    br.close();
                                    Log.i("ssample", "res " + sb.toString());
                                } else {
                                    Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.i("ssample", "Exception...");
                            }
                            DataEntryActivity.this.runOnUiThread(new C01821());
                        }

                        class C01821 implements Runnable {
                            C01821() {
                            }

                            public void run() {
                                DataEntryActivity.this.mProgress.cancel();
                                DataEntryActivity.this.finish();
                            }
                        }
                    }.start();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                //once submit disable the submit button
                findViewById(R.id.submit).setEnabled(false);

                //check daily entry is there in  offline and store the
                Date date = new Date();
                int dayOfMonth = date.getDate();
                int month = date.getMonth();
                String dateText = ((String) DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition())) + "_" + (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + (date.getYear() + 1900);
                System.out.println("dateText::" + dateText + "is contains" + DataEntryActivity.this.mDailyEntries.contains(dateText));
                if (DataEntryActivity.this.mDailyEntries.contains(dateText)) {
                    Log.i("ssample", "Daily Entry is found...");
                } else {
                    Log.i("ssample", "Daily Entry not found...");
                    DataEntryActivity.this.mDailyEntries.add(dateText);
                    Editor editor = DataEntryActivity.this.mSharedPrefs.edit();
                    editor.putStringSet("dailyEntries", DataEntryActivity.this.mDailyEntries);
                    editor.commit();
                }
                String dailyMonitorString = getDailyMonitorJsonString();

                try {

                    SharedPreferences pref = getSharedPreferences("DailyMonitorPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = pref.edit();
                    HashSet<String> set = new HashSet<String>();

                    SharedPreferences prefGetMethod = getSharedPreferences("DailyMonitorPrefs", Context.MODE_PRIVATE);
                    Set<String> allset = prefGetMethod.getStringSet("daily_monitor", null);
                    if (allset != null) {
                        List<String> sample = new ArrayList<String>(allset);
                        for (int idx = 0; idx < sample.size(); idx++) {
                            set.add(sample.get(idx));
                        }
                    }
                    set.add(dailyMonitorString);
                    edit.putStringSet("daily_monitor", set);
                    edit.commit();

                    Toast.makeText(DataEntryActivity.this, "Your data submitted offline", Toast.LENGTH_LONG).show();
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        private String getDailyMonitorJsonString() {

            JSONObject obj = new JSONObject();
            try {
                obj.put("farm", DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition()));
                obj.put("flock", DataEntryActivity.this.mFlock.getText().toString());
                obj.put("age", DataEntryActivity.this.mAge.getText().toString());
                obj.put("date", DataEntryActivity.this.mDate.getText().toString());
                obj.put("mortality", DataEntryActivity.this.mMortality.getText().toString());
                obj.put("feedtype", DataEntryActivity.this.feeds.get(DataEntryActivity.this.mFeedSelection.getSelectedItemPosition()));
                obj.put("quantity", DataEntryActivity.this.mQuantity.getText().toString());
                obj.put("water", DataEntryActivity.this.mWater.getText().toString());
                obj.put("bodyweight", DataEntryActivity.this.mWeight.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final String str = obj.toString();
            Log.i("ssample", "posting : " + str);
            return str;
        }
    }

    class C01862 extends Thread {
        C01862() {
        }

        public void run() {
            DataEntryActivity.this.loadFarmers();
        }
    }

    class C01883 implements OnTouchListener {

        C01883() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                Date date;
                if (DataEntryActivity.this.mDateText != null) {
                    String dateTxt = DataEntryActivity.this.mDateText.replace(".", "/");
                    date = new SimpleDateFormat("dd/mm/yyyy").parse(dateTxt, new ParsePosition(0));
                    date.setMonth(Integer.parseInt(dateTxt.substring(3, 5)) - 1);
                } else {
                    date = new Date();
                }
                Log.i("nayab", "date.getMonth():: " + date.getMonth());
                DataEntryActivity.this.mDatePickerDlg = new DatePickerDialog(DataEntryActivity.this, new C01871(), date.getYear() + 1900, date.getMonth(), date.getDate());
                DataEntryActivity.this.mDatePickerDlg.show();
            }
            return false;
        }

        class C01871 implements OnDateSetListener {
            C01871() {
            }

            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                DataEntryActivity.this.mDateText = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + year;
                DataEntryActivity.this.mDate.setText(DataEntryActivity.this.mDateText);
            }
        }
    }

    class C01894 implements OnItemSelectedListener {
        C01894() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            DataEntryActivity.this.loadCodeSpinner();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    class C01925 implements OnClickListener {
        C01925() {
        }

        public void onClick(View view) {
            if (!DataEntryActivity.this.mFarmerLoaded) {
                Toast.makeText(DataEntryActivity.this, "Select Farmer", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.mUnits.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Units", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.mQuantity.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Quantity", Toast.LENGTH_LONG).show();
            } else {
                String str;
                Date date = new Date();
                int dayOfMonth = date.getDate();
                int month = date.getMonth();
                int year = date.getYear() + 1900;
                StringBuilder append = new StringBuilder().append((String) DataEntryActivity.this.farmers.get(DataEntryActivity.this.mFarmerSelection.getSelectedItemPosition())).append("_").append(dayOfMonth < 10 ? "0" : "").append(dayOfMonth).append(".");
                if (month + 1 < 10) {
                    str = "0";
                } else {
                    str = "";
                }

                Iterator iterator = DataEntryActivity.this.mDailyEntries.iterator();

                // check values
                while (iterator.hasNext()) {
                    System.out.println("Value: " + iterator.next() + " ");
                }
                if (!DataEntryActivity.this.mDailyEntries.contains(append.append(str).append(month + 1).append(".").append(year).toString())) {
                    Toast.makeText(DataEntryActivity.this, "Daily Entry not happened on this date", Toast.LENGTH_SHORT).show();
                }

                //check if internet available if yes push to server
                if (DataEntryActivity.this.isNetworkAvailable()) {
                    try {

                        //once submit disable the submit button
                        findViewById(R.id.submit).setEnabled(false);
                        final String medicineVaccineString = getMedicineVaccineString();

                        DataEntryActivity.this.mProgress.dismiss();


                        new Thread() {

                            public void run() {
                                try {
                                    HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveMed.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                    Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveMed.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                    urlConnection.setDoOutput(true);
                                    urlConnection.setRequestMethod("POST");
                                    urlConnection.setUseCaches(false);
                                    urlConnection.setConnectTimeout(10000);
                                    urlConnection.setReadTimeout(10000);
                                    urlConnection.setRequestProperty("Content-Type", "application/json");
                                    urlConnection.connect();
                                    DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                    printout.writeBytes(medicineVaccineString);
                                    printout.flush();
                                    printout.close();
                                    if (urlConnection.getResponseCode() == 200) {
                                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                        StringBuffer sb = new StringBuffer();
                                        while (true) {
                                            String line = br.readLine();
                                            if (line == null) {
                                                break;
                                            }
                                            sb.append(line + "\n");
                                        }
                                        br.close();
                                        Log.i("ssample", "res " + sb.toString());
                                    } else {
                                        Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.i("ssample", "Exception...");
                                }
                                DataEntryActivity.this.runOnUiThread(new C01901());
                            }

                            class C01901 implements Runnable {
                                C01901() {
                                }

                                public void run() {
                                    DataEntryActivity.this.mProgress.cancel();
                                    DataEntryActivity.this.finish();
                                }
                            }
                        }.start();
                        return;

                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }// if offline store the values locally

                else {

                    //once submit disable the submit button
                    findViewById(R.id.submit).setEnabled(false);

                    //check whether daily entry is done or not else intimate and then store offline
                    if (!DataEntryActivity.this.mDailyEntries.contains(append.append(str).append(month + 1).append(".").append(year).toString())) {
                        Toast.makeText(DataEntryActivity.this, "Daily Entry not happened on this date", Toast.LENGTH_SHORT).show();
                    }

                    String medicine_vaccineString = getMedicineVaccineString();
                    SharedPreferences pref = getSharedPreferences("MedicineVacPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = pref.edit();
                    HashSet<String> set = new HashSet<String>();

                    SharedPreferences prefGetMethod = getSharedPreferences("MedicineVacPrefs", Context.MODE_PRIVATE);
                    Set<String> allset = prefGetMethod.getStringSet("medicine_vaccination", null);
                    if (allset != null) {
                        List<String> sample = new ArrayList<String>(allset);
                        for (int idx = 0; idx < sample.size(); idx++) {
                            set.add(sample.get(idx));
                        }
                    }
                    set.add(medicine_vaccineString);
                    edit.putStringSet("medicine_vaccination", set);
                    edit.commit();

                    Toast.makeText(DataEntryActivity.this, "Your data submitted offline", Toast.LENGTH_LONG).show();
                    finish();
                }

            }
        }
    }

    class C01936 extends Thread {
        C01936() {
        }

        public void run() {
            DataEntryActivity.this.loadFarmers();
        }
    }

    class C01957 implements OnTouchListener {

        C01957() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                Date date;
                if (DataEntryActivity.this.mDateText != null) {
                    String dateTxt = DataEntryActivity.this.mDateText.replace(".", "/");
                    date = new SimpleDateFormat("dd/mm/yyyy").parse(dateTxt, new ParsePosition(0));
                    date.setMonth(Integer.parseInt(dateTxt.substring(3, 5)) - 1);
                } else {
                    date = new Date();
                }
                Log.i("nayab", "date.getMonth():: " + date.getMonth());
                DataEntryActivity.this.mDatePickerDlg = new DatePickerDialog(DataEntryActivity.this, new C01941(), date.getYear() + 1900, date.getMonth(), date.getDate());
                DataEntryActivity.this.mDatePickerDlg.show();
            }
            return false;
        }

        class C01941 implements OnDateSetListener {
            C01941() {
            }

            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                DataEntryActivity.this.mDateText = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "." + (month + 1 < 10 ? "0" : "") + (month + 1) + "." + year;
                DataEntryActivity.this.mDate.setText(DataEntryActivity.this.mDateText);
            }
        }
    }

    class C01968 extends Thread {
        C01968() {
        }

        public void run() {
            DataEntryActivity.this.loadFarmers();
        }
    }

    class C01999 implements OnClickListener {
        C01999() {
        }

        public void onClick(View view) {
            if (!DataEntryActivity.this.mFarmerLoaded) {
                Toast.makeText(DataEntryActivity.this, "Select Farmer", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.mBirdsSold.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Birds Sold", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.mWeight.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Total Weight", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.mPrice.getText().toString().isEmpty()) {
                Toast.makeText(DataEntryActivity.this, "Enter Price per kg", Toast.LENGTH_LONG).show();
            } else if (DataEntryActivity.this.isNetworkAvailable()) {
                try {
                    //once submit disable the submit button
                    findViewById(R.id.submit).setEnabled(false);

                    //if network available push to server
                    final String birdSaleString = getBirdSaleString();

                    DataEntryActivity.this.mProgress.dismiss();
                    new Thread() {

                        public void run() {
                            try {
                                HttpURLConnection urlConnection = (HttpURLConnection) new URL(("http://bims1.spectacularsys.com/appinit/FileToSaveBirdSale.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20")).openConnection();
                                Log.i("DataEntryActivity", "" + ("http://bims1.spectacularsys.com/appinit/FileToSaveBirdSale.php?username=" + DataEntryActivity.this.mUsername + "&password=" + DataEntryActivity.this.mPassword).replace(" ", "%20"));
                                urlConnection.setDoOutput(true);
                                urlConnection.setRequestMethod("POST");
                                urlConnection.setUseCaches(false);
                                urlConnection.setConnectTimeout(10000);
                                urlConnection.setReadTimeout(10000);
                                urlConnection.setRequestProperty("Content-Type", "application/json");
                                urlConnection.connect();
                                DataOutputStream printout = new DataOutputStream(urlConnection.getOutputStream());
                                printout.writeBytes(birdSaleString);
                                printout.flush();
                                printout.close();
                                if (urlConnection.getResponseCode() == 200) {
                                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    StringBuffer sb = new StringBuffer();
                                    while (true) {
                                        String line = br.readLine();
                                        if (line == null) {
                                            break;
                                        }
                                        sb.append(line + "\n");
                                    }
                                    br.close();
                                    Log.i("ssample", "res " + sb.toString());
                                } else {
                                    Log.i("ssample", "fail " + urlConnection.getResponseMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.i("ssample", "Exception...");
                            }
                            DataEntryActivity.this.runOnUiThread(new C01971());
                        }

                        class C01971 implements Runnable {
                            C01971() {
                            }

                            public void run() {
                                DataEntryActivity.this.mProgress.cancel();
                                DataEntryActivity.this.finish();
                            }
                        }
                    }.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                //once submit disable the submit button
                findViewById(R.id.submit).setEnabled(false);

                //store the values locally
                try {
                    String birdSaleString = getBirdSaleString();


                    SharedPreferences pref = getSharedPreferences("BirdSalesPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = pref.edit();
                    HashSet<String> set = new HashSet<String>();

                    SharedPreferences prefGetMethod = getSharedPreferences("BirdSalesPrefs", Context.MODE_PRIVATE);
                    Set<String> allset = prefGetMethod.getStringSet("bird_sales", null);
                    if (allset != null) {
                        List<String> sample = new ArrayList<String>(allset);
                        for (int idx = 0; idx < sample.size(); idx++) {
                            set.add(sample.get(idx));
                        }
                    }
                    set.add(birdSaleString);
                    edit.putStringSet("bird_sales", set);
                    edit.commit();

                    Toast.makeText(DataEntryActivity.this, "Your data submitted offline", Toast.LENGTH_LONG).show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
